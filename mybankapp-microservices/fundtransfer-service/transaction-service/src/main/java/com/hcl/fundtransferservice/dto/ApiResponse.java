package com.hcl.fundtransferservice.dto;

public record ApiResponse(String message, Long httpStatus) {

}
