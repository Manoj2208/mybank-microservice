package com.hcl.fundtransferservice.kafka_producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import com.hcl.fundtransferservice.entity.FundTransfer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KafkaSender {

	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(FundTransfer fundTransfer) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, FundTransfer> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());

		log.info("Message Sent: " + fundTransfer);
		ProducerRecord<String, FundTransfer> transactionRecord = new ProducerRecord<>("transaction-queue", fundTransfer);
		log.info("" + transactionRecord);
		log.info("" + kafkaProducer.send(transactionRecord));

	}
}
