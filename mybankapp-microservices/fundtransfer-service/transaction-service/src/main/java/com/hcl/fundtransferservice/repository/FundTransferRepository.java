package com.hcl.fundtransferservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.fundtransferservice.entity.FundTransfer;

public interface FundTransferRepository extends JpaRepository<FundTransfer, String> {

}
