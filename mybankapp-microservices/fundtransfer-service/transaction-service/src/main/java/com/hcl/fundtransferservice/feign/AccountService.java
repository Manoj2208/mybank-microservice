package com.hcl.fundtransferservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.fundtransferservice.config.FeignConfiguration;
import com.hcl.fundtransferservice.dto.ApiResponse;
import com.hcl.fundtransferservice.dto.FundTransferDto;



@FeignClient(name="account-service",configuration = FeignConfiguration.class)
public interface AccountService {
	@PutMapping("api/v1/accounts")
	public ApiResponse updateAccount(@RequestBody FundTransferDto fundTransferDto);
}
