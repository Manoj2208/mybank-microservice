package com.hcl.fundtransferservice.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public record FundTransferDto(@NotNull(message = "fromAccount is required") Long fromAccountNumber,
		@NotNull(message = "toAccount is required") Long toAccountNumber,
		@Min(value = 1, message = "amount must be greater than 1") BigDecimal amount) {

}
