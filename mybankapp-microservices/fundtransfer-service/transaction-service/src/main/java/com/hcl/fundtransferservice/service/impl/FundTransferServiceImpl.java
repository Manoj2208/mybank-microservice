package com.hcl.fundtransferservice.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.hcl.fundtransferservice.dto.ApiResponse;
import com.hcl.fundtransferservice.dto.FundTransferDto;
import com.hcl.fundtransferservice.entity.FundTransfer;
import com.hcl.fundtransferservice.exception.AccountServiceException;
import com.hcl.fundtransferservice.feign.AccountService;
import com.hcl.fundtransferservice.kafka_producer.KafkaSender;
import com.hcl.fundtransferservice.repository.FundTransferRepository;
import com.hcl.fundtransferservice.service.FundTransferService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FundTransferServiceImpl implements FundTransferService {
	private final AccountService accountService;
	private final FundTransferRepository fundTransferRepository;
	private final KafkaSender kafkaSender;

	@Override
	public ApiResponse transaction(FundTransferDto fundTransferDto) {
		ApiResponse apiResponse = accountService.updateAccount(fundTransferDto);
		if (apiResponse.message().equals("Success")) {
			FundTransfer fundTransfer = FundTransfer.builder().fromAccount(fundTransferDto.fromAccountNumber())
					.toAccount(fundTransferDto.toAccountNumber()).amount(fundTransferDto.amount()).transactionDate(LocalDate.now().toString()).build();
			fundTransferRepository.save(fundTransfer);
			kafkaSender.send(fundTransfer);
			return new ApiResponse("Transaction Sucess", 201l);
		}
		throw new AccountServiceException();
	}

}
