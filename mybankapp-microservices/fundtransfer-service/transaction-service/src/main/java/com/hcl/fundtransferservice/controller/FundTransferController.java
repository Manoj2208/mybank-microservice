package com.hcl.fundtransferservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.fundtransferservice.dto.ApiResponse;
import com.hcl.fundtransferservice.dto.FundTransferDto;
import com.hcl.fundtransferservice.service.FundTransferService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/fund-transfer")
@RequiredArgsConstructor
public class FundTransferController {
	private final FundTransferService fundTransferService;

	@PostMapping
	public ResponseEntity<ApiResponse> fundTransfer(@Valid @RequestBody FundTransferDto fundTransferDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(fundTransferService.transaction(fundTransferDto));
	}
}
