package com.hcl.fundtransferservice.exception;

public class AccountServiceException extends GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountServiceException() {
		super("Account Server Error",GlobalErrorCode.ERROR_INTERNAL_SERVER);
	}
	

}
