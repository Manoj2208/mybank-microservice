package com.hcl.fundtransferservice.config;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.fundtransferservice.exception.GlobalException;

import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FeignClientErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String s, Response response) {

		return extractGlobalException(response);
	}

	/**
	 * Extracts a GlobalException object from a Response object.
	 *
	 * @param response The Response object from which to extract the
	 *                 GlobalException.
	 * @return The extracted GlobalException object, or null if extraction fails.
	 */
	private GlobalException extractGlobalException(Response response) {

		GlobalException globalException = null;
		Reader reader = null;

		try {
			reader = response.body().asReader(StandardCharsets.UTF_8);
			String result = IOUtils.toString(reader);
			log.error(result);
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			globalException = mapper.readValue(result, GlobalException.class);
			log.error(globalException.toString());
		} catch (IOException e) {
			log.error("IO Exception while reading exception message", e);
		} finally {
			if (!Objects.isNull(reader)) {
				try {
					reader.close();
				} catch (IOException e) {
					log.error("IO Exception while reading exception message", e);
				}
			}
		}
		log.info("exception:" + globalException.toString());
		return globalException;
	}

}
