package com.hcl.fundtransferservice.service;

import com.hcl.fundtransferservice.dto.ApiResponse;
import com.hcl.fundtransferservice.dto.FundTransferDto;

public interface FundTransferService {

	ApiResponse transaction(FundTransferDto fundTransferDto);

}
