package com.hcl.fundtransferservice.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FundTransfer {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private String fundTransferId;
	private Long fromAccount;
	private Long toAccount;
	private BigDecimal amount;
	private String transactionDate;
}
