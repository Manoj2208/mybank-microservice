package com.hcl.smsservice.consumer;

import java.math.BigDecimal;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SmsNotifier {
	
	@KafkaListener(topics = "transaction-queue", groupId = "group-id")
	public void sendSms(ConsumerRecord<String, Object> funds) {
		log.info("" + funds.value());
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		try {
			node = mapper.readTree(funds.value().toString());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		Long sourceAccount = node.get("fromAccount").asLong();
		BigDecimal amt = node.get("amount").decimalValue();

		String msg = String.format("An amount of INR %s has been DEBITED to your account %s", amt, sourceAccount);

		Twilio.init(System.getenv("TWILIO_ACCOUNT_SID"), System.getenv("TWILIO_AUTH_TOKEN"));
		log.info("notified through sms");
		Message.creator(new PhoneNumber("+919110810596"), new PhoneNumber(System.getenv("TWILIO_NUMBER")), msg)
				.create();

	}
}