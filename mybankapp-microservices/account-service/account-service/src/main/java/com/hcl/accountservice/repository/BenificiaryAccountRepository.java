package com.hcl.accountservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.accountservice.entity.Account;
import com.hcl.accountservice.entity.BenificiaryAccount;

public interface BenificiaryAccountRepository extends JpaRepository<BenificiaryAccount, Long> {
	Optional<BenificiaryAccount> findByBenificiaryAccountNumberAndAccount(Long benificiaryAccountNumber,
			Account account);
}
