package com.hcl.accountservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BenificiaryAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long benificiaryId;
	private Long benificiaryAccountNumber;
	private String benificiaryName;
	private String bankName;
	private String ifsc;
	@ManyToOne
	private Account account;
}
