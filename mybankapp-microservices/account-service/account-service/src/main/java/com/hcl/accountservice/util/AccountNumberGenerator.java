package com.hcl.accountservice.util;

import java.security.SecureRandom;

public class AccountNumberGenerator {
	
	public static Long generateAccountNumber() {
		SecureRandom secureRandom = new SecureRandom();
		Long accountNumber = secureRandom.nextLong();
		accountNumber = Math.abs(accountNumber % 9_000_000_000L) + 1_000_000_000L;
		return accountNumber;
	}

	private AccountNumberGenerator() {
		super();
	}

}
