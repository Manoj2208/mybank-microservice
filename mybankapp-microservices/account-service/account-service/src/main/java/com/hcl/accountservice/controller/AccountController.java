package com.hcl.accountservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.accountservice.dto.AccountDetailsDto;
import com.hcl.accountservice.dto.AccountDto;
import com.hcl.accountservice.dto.ApiResponse;
import com.hcl.accountservice.dto.FundTransferDto;
import com.hcl.accountservice.service.AccountService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/accounts")
@RequiredArgsConstructor
public class AccountController {
	private final AccountService accountService;

	@PostMapping
	public ApiResponse openAccount(@RequestBody AccountDto accountDto) {
		return accountService.openAccount(accountDto);
	}

	@GetMapping
	public ResponseEntity<AccountDetailsDto> getByAccount(@RequestParam Long accountNumber) {
		return ResponseEntity.status(HttpStatus.OK).body(accountService.getAccount(accountNumber));
	}

	@PutMapping
	public ApiResponse updateAccount(@RequestBody FundTransferDto fundTransferDto) {
		return accountService.updateAccount(fundTransferDto);

	}
}
