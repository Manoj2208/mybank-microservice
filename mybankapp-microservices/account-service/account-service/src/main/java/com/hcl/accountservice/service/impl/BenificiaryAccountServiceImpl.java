package com.hcl.accountservice.service.impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.hcl.accountservice.dto.ApiResponse;
import com.hcl.accountservice.dto.BenificiaryAccountDto;
import com.hcl.accountservice.entity.Account;
import com.hcl.accountservice.entity.BenificiaryAccount;
import com.hcl.accountservice.exception.AccountConflictException;
import com.hcl.accountservice.exception.AccountNotFoundException;
import com.hcl.accountservice.exception.UnauthorizedCustomer;
import com.hcl.accountservice.feign.CustomerService;
import com.hcl.accountservice.repository.AccountRepository;
import com.hcl.accountservice.repository.BenificiaryAccountRepository;
import com.hcl.accountservice.service.BenificiaryAccountService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BenificiaryAccountServiceImpl implements BenificiaryAccountService {
	private final BenificiaryAccountRepository benificiaryAccountRepository;
	private final AccountRepository accountRepository;
	private final CustomerService customerService;

	@Override
	public ApiResponse addBenificiary(BenificiaryAccountDto benificiaryAccountDto) {
		Account account = accountRepository.findById(benificiaryAccountDto.accountNumber())
				.orElseThrow(() -> new AccountNotFoundException(
						String.format("Account:%d not found", benificiaryAccountDto.accountNumber())));
		boolean flag = customerService.isLoggedIn(account.getCustomerId());
		if (flag) {
			Optional<BenificiaryAccount> benificiaryAccount = benificiaryAccountRepository
					.findByBenificiaryAccountNumberAndAccount(benificiaryAccountDto.benificiaryAccountNumber(),
							account);
			if (benificiaryAccount.isPresent()) {
				throw new AccountConflictException("Benificiary Already Added");
			}
			BenificiaryAccount benificiaryAccount2 = BenificiaryAccount.builder().account(account)
					.bankName(benificiaryAccountDto.bankName())
					.benificiaryAccountNumber(benificiaryAccountDto.benificiaryAccountNumber())
					.benificiaryName(benificiaryAccountDto.benificiaryName()).ifsc(benificiaryAccountDto.ifsc())
					.build();
			benificiaryAccountRepository.save(benificiaryAccount2);
			return new ApiResponse("Benificiary Added", 201l);
		}
		throw new UnauthorizedCustomer("Customer not logged in");
	}

}
