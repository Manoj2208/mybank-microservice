package com.hcl.accountservice.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
	@Id
	private Long accountNumber;
	private BigDecimal accountBalance;
	@Enumerated(EnumType.STRING)
	private AccountType accountType;
	@CreationTimestamp
	private LocalDateTime accountCreated;
	private String customerId;
}
