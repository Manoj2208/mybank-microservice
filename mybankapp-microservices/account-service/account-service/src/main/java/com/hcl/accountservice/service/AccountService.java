package com.hcl.accountservice.service;

import com.hcl.accountservice.dto.AccountDetailsDto;
import com.hcl.accountservice.dto.AccountDto;
import com.hcl.accountservice.dto.ApiResponse;
import com.hcl.accountservice.dto.FundTransferDto;

public interface AccountService {

	ApiResponse openAccount(AccountDto accountDto);

	AccountDetailsDto getAccount(Long accountNumber);

	ApiResponse updateAccount(FundTransferDto fundTransferDto);

}
