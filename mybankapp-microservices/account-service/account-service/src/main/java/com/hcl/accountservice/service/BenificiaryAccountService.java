package com.hcl.accountservice.service;

import com.hcl.accountservice.dto.ApiResponse;
import com.hcl.accountservice.dto.BenificiaryAccountDto;

public interface BenificiaryAccountService {

	ApiResponse addBenificiary(BenificiaryAccountDto benificiaryAccountDto);

}
