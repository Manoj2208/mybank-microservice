package com.hcl.accountservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.accountservice.config.FeignConfiguration;

@FeignClient(name = "customer-service",configuration = FeignConfiguration.class)
public interface CustomerService {
	@GetMapping("api/v1/customers/login/{customerId}")
	public boolean isLoggedIn(@PathVariable String customerId); 
}
