package com.hcl.accountservice.exception;

public class UnauthorizedCustomer extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnauthorizedCustomer(String message) {
		super(message, GlobalErrorCode.ERROR_UNAUTHOURIZED_USER);
	}

	public UnauthorizedCustomer() {
		super("Unauthorized Customer", GlobalErrorCode.ERROR_UNAUTHOURIZED_USER);
	}
}
