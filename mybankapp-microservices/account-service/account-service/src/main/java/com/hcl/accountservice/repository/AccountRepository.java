package com.hcl.accountservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.accountservice.entity.Account;

public interface AccountRepository extends JpaRepository<Account,Long> {

}
