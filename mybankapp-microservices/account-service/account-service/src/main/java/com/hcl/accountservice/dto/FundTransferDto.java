package com.hcl.accountservice.dto;

import java.math.BigDecimal;

public record FundTransferDto(Long fromAccountNumber, Long toAccountNumber, BigDecimal amount) {

}
