package com.hcl.accountservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.accountservice.dto.ApiResponse;
import com.hcl.accountservice.dto.BenificiaryAccountDto;
import com.hcl.accountservice.service.BenificiaryAccountService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/accounts/benificiary-account")
public class BenificiaryAccountController {
	private final BenificiaryAccountService benificiaryAccountService;

	@PostMapping
	public ResponseEntity<ApiResponse> addBenificiary(@RequestBody BenificiaryAccountDto benificiaryAccountDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(benificiaryAccountService.addBenificiary(benificiaryAccountDto));
	}

}
