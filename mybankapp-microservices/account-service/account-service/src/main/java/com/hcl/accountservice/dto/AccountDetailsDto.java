package com.hcl.accountservice.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record AccountDetailsDto(Long accountNumber,BigDecimal balance, String accountType, LocalDateTime accountCreated,
		String customerId) {
}
