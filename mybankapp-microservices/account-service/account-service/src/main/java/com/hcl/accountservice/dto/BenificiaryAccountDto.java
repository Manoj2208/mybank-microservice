package com.hcl.accountservice.dto;

public record BenificiaryAccountDto(Long accountNumber, Long benificiaryAccountNumber, String benificiaryName,
		String bankName, String ifsc) {

}
