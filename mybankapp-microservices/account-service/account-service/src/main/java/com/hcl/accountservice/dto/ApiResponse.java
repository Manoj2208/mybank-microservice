package com.hcl.accountservice.dto;


public record ApiResponse(String message, Long httpStatus) {

}
