package com.hcl.accountservice.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.accountservice.dto.AccountDetailsDto;
import com.hcl.accountservice.dto.AccountDto;
import com.hcl.accountservice.dto.ApiResponse;
import com.hcl.accountservice.dto.FundTransferDto;
import com.hcl.accountservice.entity.Account;
import com.hcl.accountservice.entity.AccountType;
import com.hcl.accountservice.entity.BenificiaryAccount;
import com.hcl.accountservice.exception.AccountNotFoundException;
import com.hcl.accountservice.exception.InsufficientFundException;
import com.hcl.accountservice.exception.UnauthorizedCustomer;
import com.hcl.accountservice.feign.CustomerService;
import com.hcl.accountservice.repository.AccountRepository;
import com.hcl.accountservice.repository.BenificiaryAccountRepository;
import com.hcl.accountservice.service.AccountService;
import com.hcl.accountservice.util.AccountNumberGenerator;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {
	private final AccountRepository accountRepository;
	private final BenificiaryAccountRepository benificiaryRepository;
	private final CustomerService customerService;

	@Override
	public ApiResponse openAccount(AccountDto accountDto) {
		Account account = Account.builder().accountNumber(AccountNumberGenerator.generateAccountNumber())
				.accountBalance(accountDto.getInitialDeposit())
				.accountType(AccountType.valueOf(accountDto.getAccountType())).customerId(accountDto.getCustomerId())
				.build();
		accountRepository.save(account);
		return new ApiResponse(String.format("Account created for customer:%s", accountDto.getCustomerId()), 201l);
	}

	@Override
	public AccountDetailsDto getAccount(Long accountNumber) {
		Account account = accountRepository.findById(accountNumber)
				.orElseThrow(() -> new AccountNotFoundException(String.format("Account:%d not found", accountNumber)));
		boolean flag = customerService.isLoggedIn(account.getCustomerId());
		if (flag) {
			return new AccountDetailsDto(account.getAccountNumber(), account.getAccountBalance(),
					account.getAccountType().toString(), account.getAccountCreated(), account.getCustomerId());
		}
		throw new UnauthorizedCustomer("Customer not logged in");
	}

	@Override
	@Transactional
	public ApiResponse updateAccount(FundTransferDto fundTransferDto) {
		Account account = accountRepository.findById(fundTransferDto.fromAccountNumber())
				.orElseThrow(() -> new AccountNotFoundException(
						String.format("Account:%d not found", fundTransferDto.fromAccountNumber())));
		boolean flag = customerService.isLoggedIn(account.getCustomerId());
		if (flag) {
			BenificiaryAccount benificiaryAccount = benificiaryRepository
					.findByBenificiaryAccountNumberAndAccount(fundTransferDto.toAccountNumber(), account)
					.orElseThrow(() -> new AccountNotFoundException(
							String.format("Account:%d not found", fundTransferDto.toAccountNumber())));
			if (account.getAccountBalance().compareTo(fundTransferDto.amount()) < 0) {
				throw new InsufficientFundException();
			}
			account.setAccountBalance(account.getAccountBalance().subtract(fundTransferDto.amount()));
			accountRepository.save(account);
			return new ApiResponse("Success", 209l);
		}
		throw new UnauthorizedCustomer("Customer not logged in");
	}

}
