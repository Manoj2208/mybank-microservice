package com.hcl.accountservice.exception;

public class AccountNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFoundException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public AccountNotFoundException() {
		super("Account Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
