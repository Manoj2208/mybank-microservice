package com.hcl.accountservice.exception;

public class AccountConflictException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountConflictException() {
		super("Customer already Registered", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public AccountConflictException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

}
