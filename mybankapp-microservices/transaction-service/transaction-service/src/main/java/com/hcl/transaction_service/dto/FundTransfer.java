package com.hcl.transaction_service.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FundTransfer {
	private String fundTransferId;
	private Long fromAccount;
	private Long toAccount;
	private BigDecimal amount;
	private LocalDate transactionDate;
}
