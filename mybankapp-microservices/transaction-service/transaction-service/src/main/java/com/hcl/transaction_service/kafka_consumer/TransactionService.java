package com.hcl.transaction_service.kafka_consumer;

import java.math.BigDecimal;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.transaction_service.entity.Transaction;
import com.hcl.transaction_service.entity.TransactionType;
import com.hcl.transaction_service.repository.TransactionRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionService {
	private final TransactionRepository transactionRepository;

	@Transactional
	@KafkaListener(topics = "transaction-queue", groupId = "group-id")
	public void consumeFundTransfer(ConsumerRecord<String, Object> funds) {
		log.info("" + funds.value());
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		try {
			node = mapper.readTree(funds.value().toString());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		Long sourceAccount = node.get("fromAccount").asLong();
		Long targetAccount = node.get("toAccount").asLong();
		BigDecimal amt = node.get("amount").decimalValue();
		Transaction debitTransaction = Transaction.builder().accountNumber(sourceAccount).amount(amt)
				.transactionType(TransactionType.DEBIT).build();
		Transaction creditTransaction = Transaction.builder().accountNumber(targetAccount).amount(amt)
				.transactionType(TransactionType.CREDIT).build();
		transactionRepository.save(debitTransaction);
		transactionRepository.save(creditTransaction);

	}
}
