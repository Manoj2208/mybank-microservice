package com.hcl.customerservice.service;

import com.hcl.customerservice.dto.ApiResponse;
import com.hcl.customerservice.dto.CustomerRecord;
import com.hcl.customerservice.dto.LoginDto;

public interface CustomerService {

	ApiResponse register(CustomerRecord customerRecord);

	ApiResponse login(LoginDto loginDto);

	ApiResponse logout(String email);

	boolean isLoggedIn(String customerId);

}
