package com.hcl.customerservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.customerservice.config.FeignConfiguration;
import com.hcl.customerservice.dto.AccountDto;
import com.hcl.customerservice.dto.ApiResponse;

@FeignClient(name = "account-service", configuration = FeignConfiguration.class)
public interface AccountService {

	@PostMapping("/api/v1/accounts")
	public ApiResponse openAccount(@RequestBody AccountDto accountDto);
}
