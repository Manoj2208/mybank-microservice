package com.hcl.customerservice.dto;


public record ApiResponse(String message, Long httpStatus) {

}
