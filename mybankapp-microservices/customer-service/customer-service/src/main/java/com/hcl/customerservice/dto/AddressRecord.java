package com.hcl.customerservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

public record AddressRecord(@NotBlank(message = "lane is required") String lane,
		@NotBlank(message = "City is required") String city, @NotBlank(message = "State is required") String state,
		@Pattern(regexp = "[1-9]{1}[0-9]{5}",message="invalid pin code") String pinCode) {

}