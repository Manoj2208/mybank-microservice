package com.hcl.customerservice.dto;

import java.math.BigDecimal;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

public record CustomerRecord(@NotBlank(message = "firstName is required") String firstName, String lastName,
		@NotBlank(message = "fatherName is required") String fatherName,
		@NotBlank(message = "contactNo is required") @Pattern(regexp = "[6-9][\\d]{9}", message = "invalid contact number") String contactNo,
		@NotNull(message = "email is required") @Email(message = "Invalid emailId") String email,
		@NotBlank(message = "Password is required") @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$@!%&*?])[A-Za-z\\d#$@!%&*?]{8,}$", message = "Password must contain atleast 1 uppercase letter, 1 lowercase letter,  1 special character, 1 number, Min 8 characters.") String password,
		@NotBlank(message = "accountType is required") String accountType,
		@NotNull(message = "specify initial deposit") @Min(value = 1000, message = "initialDeposit must greater than or equal 1000") BigDecimal initialDeposit,
		@Valid AddressRecord addressRecord) {

}
