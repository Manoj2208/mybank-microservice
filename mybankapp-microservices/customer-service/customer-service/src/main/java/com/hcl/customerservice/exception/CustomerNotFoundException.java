package com.hcl.customerservice.exception;

public class CustomerNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public CustomerNotFoundException() {
		super("Customer Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
