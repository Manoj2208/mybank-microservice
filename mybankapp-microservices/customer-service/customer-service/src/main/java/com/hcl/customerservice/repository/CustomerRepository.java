package com.hcl.customerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.customerservice.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {

}
