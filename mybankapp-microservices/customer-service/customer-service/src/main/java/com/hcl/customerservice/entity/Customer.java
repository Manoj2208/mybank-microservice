package com.hcl.customerservice.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Customer {
	@Id
	private String customerId;
	private String firstName;
	private String lastName;
	private String fatherName;
	private String contactNo;
	@OneToOne
	@Cascade(CascadeType.ALL)
	private Address address;
}
