package com.hcl.customerservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.customerservice.dto.ApiResponse;
import com.hcl.customerservice.service.CustomerService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/customers/login")
@RequiredArgsConstructor
public class LogoutController {
	private final CustomerService customerService;

	@PutMapping("/{email}")
	public ResponseEntity<ApiResponse> logout(@PathVariable String email) {
		return ResponseEntity.status(HttpStatus.OK).body(customerService.logout(email));
	}
}
