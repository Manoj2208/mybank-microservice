package com.hcl.customerservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.customerservice.dto.ApiResponse;
import com.hcl.customerservice.dto.LoginDto;
import com.hcl.customerservice.service.CustomerService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/customers/login")
@RequiredArgsConstructor
public class LoginController {
	private final CustomerService customerService;

	@PostMapping
	public ResponseEntity<ApiResponse> login(@RequestBody LoginDto loginDto) {
		return ResponseEntity.status(HttpStatus.OK).body(customerService.login(loginDto));
	}
	
	@GetMapping("/{customerId}")
	public boolean isLoggedIn(@PathVariable String customerId) {
		return customerService.isLoggedIn(customerId);
	}
}
