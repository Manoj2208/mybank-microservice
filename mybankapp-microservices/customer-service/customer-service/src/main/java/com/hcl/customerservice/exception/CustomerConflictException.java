package com.hcl.customerservice.exception;

public class CustomerConflictException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerConflictException() {
		super("Customer already Registered", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public CustomerConflictException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

}
