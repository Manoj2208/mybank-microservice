package com.hcl.customerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.customerservice.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
