package com.hcl.customerservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.customerservice.dto.ApiResponse;
import com.hcl.customerservice.dto.CustomerRecord;
import com.hcl.customerservice.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
public class RegisterController {
	private final CustomerService customerService;

	@PostMapping
	public ResponseEntity<ApiResponse> registerCustomer(@Valid @RequestBody CustomerRecord customerRecord) {
		return ResponseEntity.status(HttpStatus.CREATED).body(customerService.register(customerRecord));
	}
}
