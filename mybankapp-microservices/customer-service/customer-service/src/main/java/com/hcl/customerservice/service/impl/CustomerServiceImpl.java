package com.hcl.customerservice.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.hcl.customerservice.dto.AccountDto;
import com.hcl.customerservice.dto.AddressRecord;
import com.hcl.customerservice.dto.ApiResponse;
import com.hcl.customerservice.dto.CustomerRecord;
import com.hcl.customerservice.dto.LoginDto;
import com.hcl.customerservice.entity.Address;
import com.hcl.customerservice.entity.Customer;
import com.hcl.customerservice.entity.Login;
import com.hcl.customerservice.exception.AccountServiceException;
import com.hcl.customerservice.exception.CustomerConflictException;
import com.hcl.customerservice.exception.CustomerNotFoundException;
import com.hcl.customerservice.exception.UnauthorizedCustomer;
import com.hcl.customerservice.feign.AccountService;
import com.hcl.customerservice.repository.CustomerRepository;
import com.hcl.customerservice.repository.LoginRepository;
import com.hcl.customerservice.service.CustomerService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
	private final CustomerRepository customerRepository;
	private final LoginRepository loginRepository;
	private final AccountService accountService;

	@Override
	public ApiResponse register(CustomerRecord customerRecord) {
		Optional<Login> alreadyLogin = loginRepository.findById(customerRecord.email());
		if (alreadyLogin.isPresent()) {
			throw new CustomerConflictException();
		}
		String customerId = UUID.randomUUID().toString();
		Customer customer = Customer.builder().customerId(customerId).firstName(customerRecord.firstName())
				.lastName(customerRecord.lastName()).fatherName(customerRecord.fatherName())
				.contactNo(customerRecord.contactNo()).address(getAddress(customerRecord.addressRecord())).build();
		Login login = Login.builder().customer(customer).email(customerRecord.email())
				.password(customerRecord.password()).build();
		AccountDto accountDto = AccountDto.builder().accountType(customerRecord.accountType())
				.initialDeposit(customerRecord.initialDeposit()).customerId(customerId).build();
		customerRepository.save(customer);
		loginRepository.save(login);
		ApiResponse apiResponse = accountService.openAccount(accountDto);
		if (apiResponse != null) {
			return apiResponse;
		} else
			throw new AccountServiceException();
	}

	private Address getAddress(AddressRecord addressRecord) {
		return Address.builder().lane(addressRecord.lane()).city(addressRecord.city()).state(addressRecord.state())
				.pin(addressRecord.pinCode()).build();
	}

	@Override
	public ApiResponse login(LoginDto loginrecord) {
		Login login = loginRepository.findById(loginrecord.email()).orElseThrow(
				() -> new CustomerNotFoundException(String.format("Customer:%s not found", loginrecord.email())));
		if (login.getPassword().equals(loginrecord.password())) {
			login.setLogin(true);
			loginRepository.save(login);
			return new ApiResponse("Logged in", 200l);
		}
		throw new UnauthorizedCustomer("Invalid password");
	}

	@Override
	public boolean isLoggedIn(String customerId) {
		Customer customer = customerRepository.findById(customerId).orElseThrow(CustomerNotFoundException::new);
		Login login = loginRepository.findByCustomer(customer);
		return login.isLogin();
	}

	@Override
	public ApiResponse logout(String email) {
		Login login = loginRepository.findById(email).orElseThrow(CustomerNotFoundException::new);
		login.setLogin(false);
		loginRepository.save(login);
		return new ApiResponse("Logged out", 200l);
	}

}
