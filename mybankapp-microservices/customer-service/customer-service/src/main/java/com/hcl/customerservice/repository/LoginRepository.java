package com.hcl.customerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.customerservice.entity.Customer;
import com.hcl.customerservice.entity.Login;

public interface LoginRepository extends JpaRepository<Login, String> {
	Login findByCustomer(Customer customer);
}
